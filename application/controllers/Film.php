<?php

require APPPATH . '/libraries/REST_Controller.php';

class Film extends REST_Controller
{

    public function __construct($config = "rest"){
        parent::__construct($config);
        $this->load->model("Film_model", "film");
    }

    public function index_get(){
        $id = $this->get('id');
        $cari = $this->get('cari');
        if ($cari != "") {
            $film = $this->film->cari($cari)->result();
        } else if ($id == ''){
            $film = $this->film->getData(null)->result();   
        } else {
            $film = $this->film->getData($id)->result();
        }
        $this->response($film);
    }

    public function index_put(){
        $judul = $this->put('judul');
        $gambar = $this->put('gambar');
        $genre = $this->put('genre');
        $negara = $this->put('negara');
        $tahun = $this->put('tahun');
        $stok = $this->put('stok');
        $harga = $this->put('harga');
        $sinopsis = $this->put('sinopsis');
        $id = $this->put('id');
        $data = array(
            'judul' => $judul,
            'gambar' => $gambar,
            'genre' => $genre,
            'negara' => $negara,
            'tahun' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'sinopsis' => $sinopsis
        );
        
        $update = $this->film->update('tabel_film', $data, 'id', $this->put('id'));
        if ($update) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
        
    }

    public function index_post(){
        $judul = $this->post('judul');
        $gambar = $this->post('gambar');
        $genre = $this->post('genre');
        $negara = $this->post('negara');
        $tahun = $this->post('tahun');
        $stok = $this->post('stok');
        $harga = $this->post('harga');
        $sinopsis = $this->post('sinopsis');
        
        $data = array(
            'judul' => $judul,
            'gambar' => $gambar,
            'genre' => $genre,
            'negara' => $negara,
            'tahun' => $tahun,
            'stok' => $stok,
            'harga' => $harga,
            'sinopsis' => $sinopsis
        );
        
        $insert = $this->film->insert($data);
        if ($insert) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function index_delete(){
        $id = $this->delete('id');
        //$this->db->where('id', $id)
        $delete = $this->film->delete('tabel_film', 'id', $id);
        if ($delete) {
            $this->response(array('status' => 'success', 200));
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }
}